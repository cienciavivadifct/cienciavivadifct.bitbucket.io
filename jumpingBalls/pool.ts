/**
 * This is a pegagogical activity in Typescript, built for ExpoFCT2018
 * 
 * Team: Eduardo Geraldo, João Costa Seco, João Marques, Luis Afonso Carvalho
 * 
 */

const MAX_BALLS = 10;
const RADIUS = 10;
const GRAVITY = 1.5;
const DRAG = 0.5; // 1 - no drag, 0 - lots of drag
const BACKGROUND_COLOR = 'rgba(255, 255, 255, ' + DRAG + ')';

interface Math { sign(x: number): number; }

let canvas: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById('pool');
let context: CanvasRenderingContext2D = canvas.getContext('2d')!;

let freezing = false;
let freezeButton = <HTMLButtonElement>document.getElementById('freezeButton');
freezeButton.onclick = () => { freezing = !freezing; };

let shakeButton = <HTMLButtonElement>document.getElementById('shakeButton');
shakeButton.onclick = () => { shake() };

let gravity = false;
let gravityButton = <HTMLButtonElement>document.getElementById('gravityButton');
gravityButton.onclick = () => { gravity = !gravity; };

let colliding = false;
let collisionButton = <HTMLButtonElement>document.getElementById('collisionButton');
collisionButton.onclick = () => { colliding = !colliding; };

let numBalls = MAX_BALLS;
let ballNumInput = <HTMLInputElement>document.getElementById('number');
let ballsButton = <HTMLButtonElement>document.getElementById('setBallsButton');
ballsButton.onclick = () => {  
    window.cancelAnimationFrame(loopRef); 
    numBalls = +ballNumInput.value;
    startGame(); 
}

context.fillStyle = BACKGROUND_COLOR;

window.onload = startGame;

let balls:Ball[] = [];
function createBalls() {
	balls = []
    for (let i = 0; i < numBalls; i++) {
        balls.push(new Ball(i));
    }
}

function shake() {
    balls.forEach(ball => {
        ball.shake();
    });
}

function random(min:number, max:number) {
    var num = Math.floor(Math.random() * (max - min + 1)) + min;
    return num;
}

function rgb(r: number, g: number, b: number ) {
    return 'rgb('+r+','+g+','+b+')';
}


class Ball {
    id:number;

    x:number;
    y:number;

    color: string;
    
    dx:number;
    dy:number;

    constructor(id:number) {
        this.id = id; 

        this.x = random(0, canvas.width);
        this.y = random(0, canvas.height);

        this.dx = random(-5,5);
        if( gravity )
            this.dy = 0;
        else 
            this.dy = random(-5, 5);

        this.color = rgb(random(50, 200), random(50, 200), random(50, 200));
    }

    update() {
        
        this.x += this.dx;
        this.y += this.dy;

        if ((this.x + RADIUS) >= canvas.width) {
            this.dx = -(this.dx);
			this.x = canvas.width - RADIUS;
        }

        if ((this.x - RADIUS) <= 0) {
            this.dx = -(this.dx);
			this.x = RADIUS;
        }

        if ((this.y + RADIUS) >= canvas.height) {
            this.dy = -(this.dy);
			this.y = canvas.height - RADIUS;
        }

        if ((this.y - RADIUS) <= 0) {
            // bounce
            this.dy = -(this.dy);
			this.y = RADIUS;
        }

        // apply gravity
        if( gravity )
            this.dy += GRAVITY;

        // don't let the ball go out of the box
        this.x = Math.max(0, Math.min(this.x, canvas.width - RADIUS));
        this.y = Math.max(0, Math.min(this.y, canvas.height - RADIUS));
    }

    draw(ctx: CanvasRenderingContext2D) {
        ctx.save();
        ctx.beginPath();
        ctx.fillStyle = this.color;
        ctx.arc(this.x, this.y, RADIUS, 0, 2 * Math.PI);
        ctx.fill();
        ctx.restore();
    }

    shake() {
        this.dx = random(-20, 20);
        this.dy = random(-25, 0);
    }
}

function startGame() {
    createBalls();
    gameloop();
}

let loopRef: number = 0;
function gameloop() {
    context.fillRect(0, 0, canvas.width, canvas.height);
    
	for(var i = 0; i < balls.length; i++){
		if( !freezing ) {
            balls[i].update();

            if( colliding )
                for (var j = i + 1; j < balls.length; j++)
                    testCollision(balls[i], balls[j]);
        }

        balls[i].draw(context);
	}

	loopRef = requestAnimationFrame(gameloop);
}

function testCollision(b0 : Ball, b1: Ball) {
    if (Math.sqrt((b0.x + b0.dx - b1.x - b1.dx) * (b0.x + b0.dx - b1.x - b1.dx) + 
                  (b0.y + b0.dy - b1.y - b1.dy) * (b0.y + b0.dy - b1.y - b1.dy)) < RADIUS * 2) {	
        collideX(b0, b1);
        collideY(b0, b1);
    }
}

function collideX(ball0: Ball, ball1: Ball){
    var dxAvg = (Math.abs(ball0.dx) + Math.abs(ball1.dx)) / 2;
    
	if(Math.sign(ball0.dx) == Math.sign(ball1.dx)) { //both in the same direction, one continues the other bounces back
		if(Math.abs(ball0.dx) > Math.abs(ball1.dx)){
			ball0.dx = - Math.sign(ball0.dx) * dxAvg;
			ball1.dx =   Math.sign(ball1.dx) * dxAvg;
		}else{
			ball0.dx =   Math.sign(ball0.dx) * dxAvg;
			ball1.dx = - Math.sign(ball1.dx) * dxAvg;
		}
	}else{ //frontal collision, both bounce back
		var dxAvg = (Math.abs(ball0.dx) + Math.abs(ball1.dx)) / 2;
		if(Math.sign(ball0.dx) == 0){
			ball0.dx =   Math.sign(ball1.dx) * dxAvg;
			ball1.dx = - Math.sign(ball1.dx) * dxAvg;
		}else if(Math.sign(ball1.dx) == 0){
			ball1.dx =   Math.sign(ball0.dx) * dxAvg;
			ball0.dx = - Math.sign(ball0.dx) * dxAvg;
		}else{
			ball0.dx = - Math.sign(ball0.dx) * dxAvg;
			ball1.dx = - Math.sign(ball1.dx) * dxAvg;
		}
	}
	
	var middleX = (ball0.x + ball1.x) / 2;
	if(ball0.x < middleX){
		ball0.x = middleX - RADIUS - 1;
		ball1.x = middleX + RADIUS + 1;
	}else{
		ball0.x = middleX + RADIUS + 1;
		ball1.x = middleX - RADIUS - 1;
	}
}

function collideY(ball0: Ball, ball1: Ball){
	var dyAvg = (Math.abs(ball0.dy) + Math.abs(ball1.dy)) / 2;
	if(Math.sign(ball0.dy) == Math.sign(ball1.dy)){
		if(Math.abs(ball0.dy) > Math.abs(ball1.dy)){
			ball0.dy = - Math.sign(ball0.dy) * dyAvg;
			ball1.dy =   Math.sign(ball1.dy) * dyAvg;
		}else{
			ball0.dy =   Math.sign(ball0.dy) * dyAvg;
			ball1.dy = - Math.sign(ball1.dy) * dyAvg;
		}
	}else{
		if(Math.sign(ball0.dy) == 0){
			ball0.dy =   Math.sign(ball1.dy) * dyAvg;
			ball1.dy = - Math.sign(ball1.dy) * dyAvg;
		}else if(Math.sign(ball1.dy) == 0){
			ball1.dy =   Math.sign(ball0.dy) * dyAvg;
			ball0.dy = - Math.sign(ball0.dy) * dyAvg;
		}else{
			ball0.dy = - Math.sign(ball0.dy) * dyAvg;
			ball1.dy = - Math.sign(ball1.dy) * dyAvg;
		}
	}
}
