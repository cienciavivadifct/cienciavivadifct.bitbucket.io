"use strict";
/**
 * This is a pegagogical activity in Typescript, built for ExpoFCT2018
 *
 * Team: Eduardo Geraldo, João Costa Seco, João Marques, Luis Afonso Carvalho
 *
 */
var evalButton = document.getElementById('evalButton');
var code_box = document.getElementById('codebox');
var code_log = document.getElementById('code_log');

code_box.onkeydown = function(e) {
	if(e.ctrlKey && e.keyCode == 13) {
		evaluateCode();
	}
}

function evaluateCode () {
    var code = [code_box.value];
	
	if (code[0].length <= 0) {
		return;
	}
	
	var evaled = true;
	var result;
	
	try {
		result = eval.apply(this, code);
	} catch (e) {
		evaled = false;
		result = e;
	}
	
	if (evaled) {
		code_box.value = "";
		addGreenLog(code[0], result);
	} else {
		addRedLog(code[0], result);
	}
		
};

evalButton.onclick = evaluateCode;

function addGreenLog(code, res) {
	var div = newLog(code, res);
	div.className = "alert alert-success"
	//div.className = "ok_log";
}

function addRedLog(code, error) {
	var div = newLog(code, error);
	div.className = "alert alert-danger"
	//div.className = "error_log";
}

function newLog(code, res) {
	var log = document.createElement('div')
	
	var expr = document.createElement('div');
	expr.innerHTML = code;
	
	var result = document.createElement('div');
	result.innerHTML = res;
	
	log.appendChild(expr);
	log.appendChild(result);
	code_log.appendChild(log);
	
	code_log.scrollTop = code_log.scrollHeight;
	
	return log;
}