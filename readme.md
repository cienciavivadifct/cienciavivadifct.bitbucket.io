# This is the repository containing the activities for the 2018 ExpoFCT

There are two activities here that introduce the use of HTML, CSS, and Javascript as the
tools for building web applications. 

## naWeb

This is a simple demo of building a webpage with HTML. The file `Readme.md` contains a detailed tutorial on how to build the page, change its appearance using stylesheets (CSS), and add behaviour to it using Javascript

## JumpingBalls

This is a demo on how to use HTML, CSS and [Typescript](https://www.typescriptlang.org/) as a way to implement a small physical simulation. A tutorial is found on the `index.html` file, which is also the starting point for this example.
